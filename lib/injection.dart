import 'package:briix_assignment/app_router.dart';
import 'package:briix_assignment/cache/data/movie_cache.dart';
import 'package:briix_assignment/domain/use_case/create_movie_usecase.dart';
import 'package:briix_assignment/domain/use_case/delete_movie_usecase.dart';
import 'package:briix_assignment/domain/use_case/get_movie_detail_usecase.dart';
import 'package:briix_assignment/domain/use_case/search_movies_usecase.dart';
import 'package:briix_assignment/domain/use_case/update_movie_use_case.dart';
import 'package:briix_assignment/presenter/movie_detail_presenter.dart';
import 'package:briix_assignment/presenter/movie_list_presenter.dart';
import 'package:briix_assignment/repository/movie_repository.dart';
import 'package:get_it/get_it.dart';

final getIt = GetIt.instance;

void setUpGetIt() {

  getIt.registerFactory<AppRouter>(() => AppRouter());

  getIt.registerFactory<MovieDetailPresenter>(() => MovieDetailPresenter());
  getIt.registerFactory<MovieListPresenter>(() => MovieListPresenter());

  getIt.registerFactory<CreateMovieUseCase>(() => CreateMovieUseCase());
  getIt.registerFactory<DeleteMovieUseCase>(() => DeleteMovieUseCase());
  getIt.registerFactory<GetMovieDetailUseCase>(() => GetMovieDetailUseCase());
  getIt.registerFactory<SearchMoviesUseCase>(() => SearchMoviesUseCase());
  getIt.registerFactory<UpdateMovieUseCase>(() => UpdateMovieUseCase());

  getIt.registerFactory<MovieRepository>(() => MovieRepository());

  getIt.registerSingleton<MovieCache>(MovieCache());


}