import 'package:auto_route/auto_route.dart';
import 'package:briix_assignment/app_router.dart';
import 'package:briix_assignment/domain/model/movie_model.dart';
import 'package:briix_assignment/injection.dart';
import 'package:briix_assignment/presenter/movie_list_presenter.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

@RoutePage()
class MovieListScreen extends StatefulWidget {
  const MovieListScreen({super.key});

  @override
  State<MovieListScreen> createState() => _MovieListScreenState();
}

class _MovieListScreenState extends State<MovieListScreen> {

  final _presenter = getIt.get<MovieListPresenter>();
  late TextEditingController _searchController;

  @override
  void initState() {
    _searchController = TextEditingController();
    super.initState();
    _presenter.searchMovies("");
  }

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Movies Collection",
          style: TextStyle(
            color: Colors.white
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.teal,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => _createMovie(),
        backgroundColor: Colors.teal,
        child: const Icon(Icons.add, color: Colors.white),
      ),
      body: Column(
        children: [
          _searchWidget(),
          Container(
            height: 3,
            color: Colors.teal.shade50,
          ),
          Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: Observer(
                  builder: (context) {
                    return _movieList();
                  }
                ),
              )
          ),
        ],
      ),
    );
  }

  Widget _searchWidget() {
    return Padding(
      padding: const EdgeInsets.symmetric(
          vertical: 8,
          horizontal: 16
      ),
      child: TextField(
        textAlign: TextAlign.center,
        controller: _searchController,
        decoration: const InputDecoration(
          contentPadding: EdgeInsets.zero,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(16)),
          ),
          hintText: 'Search by title...',
        ),
        onChanged: (value) {
          _presenter.searchMovies(value);
        },
      ),
    );
  }

  Widget _movieList() {
    if (kIsWeb) {
      return GridView.builder(
        gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
            maxCrossAxisExtent: 200.0,
            crossAxisSpacing: 8.0,
            mainAxisSpacing: 8.0,
            childAspectRatio: 3 / 2
        ),
        itemBuilder: (context, index) {
          return _moviePreView(_presenter.movies[index]);
        },
        itemCount: _presenter.movies.length,
      );
    } else {
      return ListView.builder(
        padding: const EdgeInsets.only(bottom: 75.0),
        itemBuilder: (context, index) {
          return _moviePreView(_presenter.movies[index]);
        },
        itemCount: _presenter.movies.length,
      );
    }
  }

  Widget _moviePreView(MovieModel model) {
    String genres = "";
    for (var element in model.genres) {
      if (genres == "") {
        genres = element;
      } else {
        genres += " / $element";
      }
    }

    return GestureDetector(
      onTap: () async {
        final result = await context.router.push(MovieDetailRoute(movieId: model.id)) ?? false;
        if (result is bool && result) {
          _presenter.searchMovies(_searchController.text);
        }
      },
      child: Card(
        color: Colors.teal.shade50,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Text(model.title,
                textAlign: TextAlign.left,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: const TextStyle(
                  fontWeight: FontWeight.bold
                ),
              ),
              kIsWeb
                ? Expanded(
                  child: Text(model.director,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  )
                )
                :Text(model.director,
              ),
              Text(genres,
                textAlign: TextAlign.right,
                maxLines: 2,
                overflow: TextOverflow.fade,
                style: const TextStyle(
                  color: Colors.black54,
                  fontSize: 12
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _createMovie() async {
    final result = await context.router.push(MovieDetailRoute(movieId: 0)) ?? false;
    if (result is bool && result) {
      _presenter.searchMovies(_searchController.text);
    }
  }
}
