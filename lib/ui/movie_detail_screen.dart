import 'package:auto_route/auto_route.dart';
import 'package:briix_assignment/injection.dart';
import 'package:briix_assignment/presenter/movie_detail_presenter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

@RoutePage()
class MovieDetailScreen extends StatefulWidget {
  const MovieDetailScreen({super.key, required this.movieId});

  final int movieId;

  @override
  State<MovieDetailScreen> createState() => _MovieDetailScreenState();
}

class _MovieDetailScreenState extends State<MovieDetailScreen> {

  final _presenter = getIt.get<MovieDetailPresenter>();
  late TextEditingController _titleController;
  late TextEditingController _directorController;
  late TextEditingController _summaryController;

  @override
  void initState() {
    _titleController = TextEditingController();
    _directorController = TextEditingController();
    _summaryController = TextEditingController();
    super.initState();
    if (widget.movieId > 0) {
      _presenter.loadMovie(widget.movieId);
    }
  }

  @override
  void dispose() {
    _titleController.dispose();
    _directorController.dispose();
    _summaryController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.teal,
        leading: IconButton(
            onPressed: _backAction,
            icon: const Icon(Icons.arrow_back,
              color: Colors.white)
        ),
        actions: [
          widget.movieId > 0 ?
          IconButton(
              onPressed: _deleteAction,
              icon: const Icon(Icons.delete,
                color: Colors.white)
          ): Container(),
          IconButton(
              onPressed: _saveAction,
              icon: const Icon(Icons.send,
                color: Colors.white
              )
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Observer(
          builder: (context) {
            if (widget.movieId > 0) {
              if (_titleController.text == "") {
                _titleController.text = _presenter.movie.title;
              }
              if (_directorController.text == "") {
                _directorController.text = _presenter.movie.director;
              }
              if (_summaryController.text == "") {
                _summaryController.text = _presenter.movie.summary;
              }
            }
            return Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                _inputTitleWidget(),
                _inputDirectorWidget(),
                _inputSummaryWidget(),
                _tagWidget()
              ]
            );
          }
        ),
      ),
    );
  }

  Widget _inputTitleWidget() {
    return _inputWidget(
        "Title",
        _titleController
    );
  }

  Widget _inputDirectorWidget() {
    return _inputWidget(
        "Director",
        _directorController
    );
  }

  Widget _inputSummaryWidget() {
    return _inputWidget(
      "Summary",
      _summaryController,
      minLines: 5,
      maxLines: 10
    );
  }

  Widget _inputWidget(String label,
      TextEditingController controller,
      {int? minLines, int? maxLines}) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: TextField(
        controller: controller,
        minLines: minLines,
        maxLines: maxLines,
        decoration: InputDecoration(
          contentPadding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
          labelText: label,
          border: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(8)),
          ),
        ),
      ),
    );
  }

  Widget _tagWidget() {
    return Padding( //outer spacing
      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
      child: Wrap(
        spacing: 8, // space between items
        children: _presenter.availableTags.map((genre) {
            bool isSelected = _presenter.selectedTag.contains(genre);
            return Container(
              padding: const EdgeInsets.all(8),
              decoration: BoxDecoration(
                color: isSelected ? Colors.teal : Colors.grey.shade100,
                borderRadius: BorderRadius.circular(4),
              ),
              child: GestureDetector(
                  onTap: () {
                    _presenter.onSelectGenre(genre);
                  },
                  child: Text(genre,
                    style: TextStyle(
                      color: isSelected ? Colors.white : Colors.black,
                    ),
                  )
              ),
            );
          }
        ).toList(),
      ),
    );
  }

  void _backAction() {
    context.router.pop(false);
  }

  Future<void> _deleteAction() async {
    await _presenter.deleteMovie(widget.movieId);
    if(context.mounted) {
      context.router.pop(true);
    }
  }

  Future<void> _saveAction() async {
    await _presenter.saveMovie(
      widget.movieId,
      _titleController.text,
      _directorController.text,
      _summaryController.text
    );

    if(context.mounted) {
      context.router.pop(true);
    }
  }
}
