
class MovieModel {
  int id = 0;
  String title = "";
  String director = "";
  String summary = "";
  List<String> genres = [];

  MovieModel({
    this.id = 0,
    this.title = "",
    this.director = "",
    this.summary = "",
    this.genres = const []});
}