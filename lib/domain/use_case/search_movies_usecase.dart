import 'package:briix_assignment/cache/model/movie_entity.dart';
import 'package:briix_assignment/domain/model/movie_model.dart';
import 'package:briix_assignment/domain/use_case/base_use_case.dart';
import 'package:briix_assignment/injection.dart';
import 'package:briix_assignment/repository/movie_repository.dart';

class SearchMoviesUseCase extends BaseUseCase<String, List<MovieModel>> {
  
  final _repository = getIt.get<MovieRepository>();
  
  @override
  Future<List<MovieModel>> execute(String t) async {
    var entities = await _repository.searchMovie(t);
    return entities.map((e) => _entityToModel(e)).toList();
  }

  MovieModel _entityToModel(MovieEntity entity) {
    MovieModel model = MovieModel();
    model.id = entity.id;
    model.title = entity.title;
    model.director = entity.director;
    model.summary = entity.summary;
    model.genres = entity.genres;
    return model;
  }

}