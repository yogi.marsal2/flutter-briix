import 'package:briix_assignment/cache/model/movie_entity.dart';
import 'package:briix_assignment/domain/model/movie_model.dart';
import 'package:briix_assignment/domain/use_case/base_use_case.dart';
import 'package:briix_assignment/injection.dart';
import 'package:briix_assignment/repository/movie_repository.dart';

class CreateMovieUseCase extends BaseUseCase<MovieModel, MovieModel> {

  final _repository = getIt.get<MovieRepository>();

  @override
  Future<MovieModel> execute(MovieModel t) async {
    MovieEntity entity = MovieEntity();
    entity.id = DateTime.now().millisecond;
    entity.title = t.title;
    entity.director = t.director;
    entity.summary = t.summary;
    entity.genres = t.genres;
    final savedVal = await _repository.addMovie(entity);
    t.id = savedVal.id;
    return t;
  }

}