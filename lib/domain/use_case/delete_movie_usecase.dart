import 'package:briix_assignment/domain/use_case/base_use_case.dart';
import 'package:briix_assignment/injection.dart';
import 'package:briix_assignment/repository/movie_repository.dart';

class DeleteMovieUseCase extends BaseUseCase<int, bool> {

  final _repository = getIt.get<MovieRepository>();

  @override
  Future<bool> execute(int t) async {
    return _repository.deleteMovie(t);
  }

}