abstract class BaseUseCase<T,K> {
  Future<K> execute(T t);
}