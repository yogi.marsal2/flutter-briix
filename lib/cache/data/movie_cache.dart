import 'package:briix_assignment/cache/model/movie_entity.dart';

class MovieCache {
  List<MovieEntity> movies = [];

  Future<MovieEntity> addMovie(MovieEntity model) async {
    movies.add(model);
    movies.sort((a, b) => a.title.compareTo(b.title));
    return model;
  }

  Future<MovieEntity> updateMovie(MovieEntity model) async {
    for (var element in movies) {
      if (element.id == model.id) {
        element.title = model.title;
        element.director = model.director;
        element.summary = model.summary;
        element.genres = model.genres;
      }
    }
    movies.sort((a, b) => a.title.compareTo(b.title));
    return model;
  }

  Future<MovieEntity> getMovieDetail(int movieId) async {
    return movies.firstWhere((element) => element.id == movieId);
  }

  Future<bool> deleteMovie(int movieId) async {
    movies.removeWhere((element) => element.id == movieId);
    return true;
  }

  Future<List<MovieEntity>> searchMovie(String searchKey) async {
    List<MovieEntity> result = [];
    if (searchKey == "") {
      return movies;
    }

    List<String> searchKeys = searchKey.split(" ");
    for (var element in movies) {
      for(var sk in searchKeys) {
        if (element.title.contains(sk)) {
          result.add(element);
          break;
        }
      }
    }
    result.sort((a, b) => a.title.compareTo(b.title));
    return result;
  }
}