class MovieEntity {
  int id = 0;
  String title = "";
  String director = "";
  String summary = "";
  List<String> genres = [];

  MovieEntity() {
    id = DateTime.now().millisecond;
  }
}