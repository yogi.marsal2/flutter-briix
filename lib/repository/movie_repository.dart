import 'package:briix_assignment/cache/data/movie_cache.dart';
import 'package:briix_assignment/cache/model/movie_entity.dart';
import 'package:briix_assignment/injection.dart';

class MovieRepository {

  final _cache = getIt.get<MovieCache>();

  Future<MovieEntity> addMovie(MovieEntity model) {
    return _cache.addMovie(model);
  }

  Future<MovieEntity> updateMovie(MovieEntity model) {
    return _cache.updateMovie(model);
  }

  Future<MovieEntity> getMovieDetail(int movieId) {
    return _cache.getMovieDetail(movieId);
  }

  Future<bool> deleteMovie(int movieId) {
    return _cache.deleteMovie(movieId);
  }

  Future<List<MovieEntity>> searchMovie(String searchKey) {
    return _cache.searchMovie(searchKey);
  }

}