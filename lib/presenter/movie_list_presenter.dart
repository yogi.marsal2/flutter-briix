import 'package:briix_assignment/domain/model/movie_model.dart';
import 'package:briix_assignment/domain/use_case/search_movies_usecase.dart';
import 'package:briix_assignment/injection.dart';
import 'package:mobx/mobx.dart';

part 'movie_list_presenter.g.dart';

class MovieListPresenter = MovieListPresenterBase with _$MovieListPresenter;

abstract class MovieListPresenterBase with Store {

  final _searchMovieUseCase = getIt.get<SearchMoviesUseCase>();

  @observable
  ObservableList<MovieModel> movies = ObservableList<MovieModel>();

  @observable
  String onProgressSearchKey = "";

  @action
  Future<void> searchMovies(String searchKey) async {
    onProgressSearchKey = searchKey;
    final searchedMovies = await _searchMovieUseCase.execute(searchKey);
    if (onProgressSearchKey == searchKey) {
      movies.clear();
      movies.addAll(searchedMovies);
    }
  }
}