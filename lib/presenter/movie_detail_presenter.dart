import 'package:briix_assignment/domain/model/movie_model.dart';
import 'package:briix_assignment/domain/use_case/create_movie_usecase.dart';
import 'package:briix_assignment/domain/use_case/delete_movie_usecase.dart';
import 'package:briix_assignment/domain/use_case/get_movie_detail_usecase.dart';
import 'package:briix_assignment/domain/use_case/update_movie_use_case.dart';
import 'package:briix_assignment/injection.dart';
import 'package:mobx/mobx.dart';

part 'movie_detail_presenter.g.dart';

class MovieDetailPresenter = MovieDetailPresenterBase with _$MovieDetailPresenter;

abstract class MovieDetailPresenterBase with Store {

  final _createMovieUseCase = getIt.get<CreateMovieUseCase>();
  final _updateMovieUseCase = getIt.get<UpdateMovieUseCase>();
  final _deleteMovieUseCase = getIt.get<DeleteMovieUseCase>();
  final _getMovieDetailUseCase = getIt.get<GetMovieDetailUseCase>();

  @observable
  MovieModel movie = MovieModel();

  List<String> availableTags = ["Action", "Animation", "Drama", "Sci-Fi"];

  @observable
  ObservableList<String> selectedTag = ObservableList<String>();

  @action
  Future<void> loadMovie(int id) async {
    movie = await _getMovieDetailUseCase.execute(id);
    selectedTag.addAll(movie.genres);
  }

  @action
  Future<void> saveMovie(int movieId, String title, String director, String summary) async{
    if (movieId == 0) {
      _createMovieUseCase.execute(MovieModel(
        title: title,
        director: director,
        summary: summary,
        genres: selectedTag.toList(),
      ));
    } else {
      _updateMovieUseCase.execute(MovieModel(
        id: movieId,
        title: title,
        director: director,
        summary: summary,
        genres: selectedTag.toList(),
      ));
    }
  }

  @action
  Future<void> deleteMovie(int id) async {
    _deleteMovieUseCase.execute(id);
  }

  @action
  void onSelectGenre(String genre) {
    if (selectedTag.contains(genre)) {
      selectedTag.remove(genre);
    } else {
      selectedTag.add(genre);
    }
  }
}