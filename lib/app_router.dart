
import 'package:auto_route/auto_route.dart';
import 'package:briix_assignment/ui/movie_detail_screen.dart';
import 'package:briix_assignment/ui/movie_list_screen.dart';
import 'package:flutter/material.dart';
part 'app_router.gr.dart';


@AutoRouterConfig()
class AppRouter extends _$AppRouter {

  @override
  List<AutoRoute> get routes => [
    AutoRoute(page: MovieListRoute.page, initial: true),
    AutoRoute(page: MovieDetailRoute.page),
  ];

}